


#include <stdexcept>
#include <exception>
#include <iterator>
#include <iostream>

#include <configfile.h>
#include <config/InMemoryDatabase.h>
#include <config/Overlay.h>


int main()
{
	try
	{
		{
			config::InMemoryDatabase file_config;
			{
				file_config.storeInt("blub", 42);
				file_config.storeFloat("blab", 23.0f);
				file_config.storeString("bli", "blo");

				auto& tatooine = file_config.openNode("tatooine");
				{
					tatooine.storeString("name", "Tatooine");
					tatooine.storeInt("num_suns", 2);
					auto& mos_eisley = tatooine.openNode("mos_eisley");
					{
						mos_eisley.storeString("name", "Mos Eisley");
					}
				}
			}

			config::InMemoryDatabase arg_config;
			{
				arg_config.storeInt("blub", 0);
				auto& tatooine = arg_config.openNode("dagobah");
				{
					tatooine.storeString("name", "Dagobah");
				}
			}


			config::DatabaseOverlay config(file_config, { &arg_config });

			auto& n = static_cast<const config::Database&>(config).queryNode("n");

			std::cout << "-------------------------- config ------------------------------\n";
			configfile::write(std::cout, config);
			std::cout << "--------------------------- args -------------------------------\n";
			configfile::write(std::cout, arg_config);
			std::cout << "--------------------------- file -------------------------------\n";
			configfile::write(std::cout, file_config);

			configfile::write("bla.cfg", file_config);
		}

		{
			std::cout << "\n\n----------------------------------------------------------------\n";
			config::InMemoryDatabase config;

			try
			{
				configfile::read(config, "null.cfg");
				throw std::logic_error("opening 'null.cfg' should fail!");
			}
			catch (const configfile::read_error&)
			{
			}

			try
			{
				constexpr const char inline_source[] = "23\nmikey = 42\n";
				configfile::read(config, inline_source, inline_source + 10, "mikey.cfg");
			}
			catch (const configfile::read_error&)
			{
			}

			configfile::read(config, "bla.cfg");
			configfile::write(std::cout, config);
		}
	}
	catch (const std::exception& e)
	{
		std::cerr << "error: " << e.what() << std::endl;
		return -1;
	}
	catch (...)
	{
		std::cerr << "unknown exception\n";
		return -128;
	}

	return 0;
}
